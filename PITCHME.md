# SYRIUS-35344
+++
## Userstory
<img src="http://drive.google.com/uc?export=view&id=14yDp337NsI7Pw99PX9xu8_8WX9Dtt2xR" width="70%"/>
+++
## XML
<img src="http://drive.google.com/uc?export=view&id=15gQt14cfnznH2ua81krEutzGcVqjUw3I" width="70%"/>
---
# SYRIUS-36983
+++
## Userstory
<img src="http://drive.google.com/uc?export=view&id=1Zve_5WpW35IAu43FW7WNny3qYF4GWQgJ" width="70%"/>
+++
## GUI-Anpassungen
+++
## Vorher
<img src="http://drive.google.com/uc?export=view&id=1Gg8Nf2-tb3FhidyCAeJxwGsgoxnUQk05" width="70%"/>
+++
## Nachher
<img src="http://drive.google.com/uc?export=view&id=1_Px2mT4f2NuK4hm3IOne46URI3b40j6d" width="70%"/>
+++
## Batch Default Personengruppen-Definitionen setzen
+++
## Ausgangslage
<img src="http://drive.google.com/uc?export=view&id=1gvaAsRRk1MIELMVXLZGBYyYCLKUwqoWB" width="70%"/>
+++
## Batch wird ausgef&uuml;hrt
+++
## Resultat
<img src="http://drive.google.com/uc?export=view&id=1eW5NGR-W4nk2G2xkbkkCu8nGSrWPEy4j" width="70%"/>